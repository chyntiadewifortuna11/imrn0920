var nama = "Dewi"
var peran = "Werewolf"

if (nama == ""){
    console.log("Nama harus diisi!")
}else if(nama != "" && peran == ""){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
}else{
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    if(peran == "Penyihir"){
        console.log("Halo Penyihir " + nama +", kamu dapat melihat siapa yang menjadi werewolf!");
    }else if(peran == "Guard"){
        console.log("Halo Guard " + nama +", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    }
    else if(peran == "Werewolf"){
        console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!")
    }
}

//Switch Case
var hari = 11; 
var bulan = 2; 
var tahun = 2000;

switch(bulan){
    case 1 : bulans = "Januari"; break;
    case 2 : bulans = "Februari"; break;
    case 3 : bulans = "Maret"; break;
    case 4 : bulans = "April"; break;
    case 5 : bulans = "Mei"; break;
    case 6 : bulans = "Juni"; break;
    case 7 : bulans = "Juli"; break;
    case 8 : bulans = "Agustus"; break;
    case 9 : bulans = "September"; break;
    case 10 : bulans = "Oktober"; break;
    case 11 : bulans = "November"; break;
    case 12 : bulans = "Desember"; break;
}

console.log(hari + " " + bulans + " " + tahun)