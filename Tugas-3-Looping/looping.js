//no 1 looping while

flag = 0

console.log('LOOPING PERTAMA')
while(flag < 20){    
    flag += 2
    console.log(flag + ' - I love coding')
}

console.log('LOOPING KEDUA')
while(flag > 2){
    flag -= 2
    console.log(flag + ' - I love coding')
}

console.log("============SOAL2================")

//no 2 looping for
var out = ""
for(angka = 1; angka < 21; angka++){
    if(angka%3 == 0 && angka%2 !=0){
        out = "I Love Coding"
    }else if(angka%2 != 0){
        out = "Santai"
    }else if(angka%2 == 0){
        out = "Berkualitas"
    }
    console.log(angka + ' - ' + out)
}

console.log("============SOAL3================")

var char = ""
for(i = 0; i < 4; i++){
    for(j = 0; j < 8; j++){
        char += ("#")
    }
    console.log(char);
    char=""
}

console.log("============SOAL4================")

var char = ""
for(i = 0; i < 7; i++){
    for(j = 0; j < i; j++){
        char += ("#")
    }
    console.log(char);
    char=""
}

console.log("============SOAL5================")
var char = ""
for(i = 0; i < 8; i++){
    for(j = 0; j < 8; j++){
        if(i%2 != 0){
            if(j%2 == 0){
                char += (" ")
            }else{
                char += ("#")
            }
        }else{
            if(j%2 == 0){
                char += ("#")
            }else{
                char += (" ")
            }
        }
    }
    console.log(char);
    char=""
}