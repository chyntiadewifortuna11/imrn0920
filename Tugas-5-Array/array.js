//soal 1

function range(num1,num2){
    var nums = []
    if(num1 > num2){
        for(i = num1;i>=num2;i--){
            nums.push(i)
        }
        return nums
    }else if(num1 < num2){
        for(i = num1;i<=num2;i++){
            nums.push(i)
        }
        return nums
    }else{
        return -1
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//soal2
console.log('=======SOAL2========')
function rangeWithStep(startNum, finishNum, step){
    var nums = []
    if(startNum < finishNum){
        for(startNum; startNum<=finishNum; startNum+=step){
            nums.push(startNum)
        }
        return nums
    }else if(startNum > finishNum){
        for(startNum; startNum>=finishNum ; startNum-=step){
            nums.push(startNum)
        }
        return nums
    }else{
        return -1
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//soal3
console.log('=======SOAL3========')
function sum(startNum=0,finishNum=0,step=1){
    var total = 0
    if(startNum < finishNum){
        for(startNum; startNum<=finishNum; startNum+=step){
            total += startNum 
        }
        return total
    }else if(startNum > finishNum){
        for(startNum; startNum>=finishNum ; startNum-=step){
            total += startNum 
        }
        return total
    }else{
        return 0
    }
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//soal4
console.log('=======SOAL4========')

function dataHandling(input){
    for(i=0;i<input.length;i++){
        a = input[i]
        for(j=0;j<a.length;j++){
            //console.log(input[i][j])
            if(j == 1){
                console.log("Nomor ID: " + input[i][1])
            }
            if(j == 2){
                console.log("Nama Lengkap : " + input[i][2])
            }
            if(j == 3){
                console.log("TTL : " + input[i][3])
            }
            if(j == 4){
                console.log("Hobi : " + input[i][4])
            }
        }
        console.log("")
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

dataHandling(input)
//soal5
console.log('=======SOAL5========')

function balikKata(kata){
    return kata.split("").reverse().join("")
} 

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
//soal6
console.log('=======SOAL6========')
var newTanggal = []
function dataHandling2(inp){
    inp.splice(1,1,"Roman Alamsyah Elsharawy")
    inp.splice(2,1,"Provinsi Bandar Lampung")
    inp.splice(4,1, "Pria", "SMA Internasional Metro")
    console.log(inp)
    var tanggal = inp[3].split("/")
    switch(tanggal[1]){
        case '01':   { console.log(' Januari '); break; }
        case '02':   { console.log(' Februari '); break; }
        case '03':   { console.log(' Maret '); break; }
        case '04':   { console.log(' April '); break; }
        case '05':   { console.log(' Mei '); break; }
        case '06':   { console.log(' Juni '); break; }
        case '07':   { console.log(' Juli '); break; }
        case '08':   { console.log(' Agustus '); break; }
        case '09':   { console.log(' September '); break; }
        case '10':   { console.log(' Oktober '); break; }
        case '11':   { console.log(' November '); break; }
        case '12':   { console.log(' Desember '); break; }
        default:  { console.log('Salah Input'); }
     }
     tanggal.sort((x,y) => y - x)
     console.log(tanggal)
     tanggal.sort((x,y) => x - y)
     var temp = tanggal[0]
     tanggal[0] = tanggal[1]
     tanggal[1] = temp
     div = tanggal.join("-")
     console.log(div)
     name = inp[1].slice(0,14)
     console.log(name)
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
